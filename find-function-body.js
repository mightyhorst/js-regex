const regex = /^\s*[\w\s]+\(.*\)\s*\K({((?>"(?:[^"\\]*+|\\.)*"|'(?:[^'\\]*+|\\.)*'|//.*$|/\*[\s\S]*?\*/|#.*$|<<<\s*["']?(\w+)["']?[^;]+\3;$|[^{}<'"/#]++|[^{}]++|(?1))*)})/gm;
const str = `<?php

namespace Illuminate\\Database\\Eloquent;

use Closure;
use DateTime;
use Exception;
use ArrayAccess;
use Carbon\\Carbon;
use LogicException;
use JsonSerializable;
use DateTimeInterface;
use Illuminate\\Support\\Arr;
use Illuminate\\Support\\Str;
use InvalidArgumentException;
use Illuminate\\Contracts\\Support\\Jsonable;
use Illuminate\\Contracts\\Events\\Dispatcher;
use Illuminate\\Contracts\\Support\\Arrayable;
use Illuminate\\Contracts\\Routing\\UrlRoutable;
use Illuminate\\Contracts\\Queue\\QueueableEntity;
use Illuminate\\Database\\Eloquent\\Relations\\Pivot;
use Illuminate\\Database\\Eloquent\\Relations\\HasOne;
use Illuminate\\Database\\Eloquent\\Relations\\HasMany;
use Illuminate\\Database\\Eloquent\\Relations\\MorphTo;
use Illuminate\\Database\\Eloquent\\Relations\\Relation;
use Illuminate\\Database\\Eloquent\\Relations\\MorphOne;
use Illuminate\\Support\\Collection as BaseCollection;
use Illuminate\\Database\\Eloquent\\Relations\\MorphMany;
use Illuminate\\Database\\Eloquent\\Relations\\BelongsTo;
use Illuminate\\Database\\Query\\Builder as QueryBuilder;
use Illuminate\\Database\\Eloquent\\Relations\\MorphToMany;
use Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany;
use Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough;
use Illuminate\\Database\\ConnectionResolverInterface as Resolver;

abstract class Model implements ArrayAccess, Arrayable, Jsonable, JsonSerializable, QueueableEntity, UrlRoutable
{
    does(){
        'hello world'
    }

    thisy(){
        'helo again'
    }

    work(){

        'hello once more';
    }
}`;
let m;

while ((m = regex.exec(str)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regex.lastIndex) {
        regex.lastIndex++;
    }
    
    // The result can be accessed through the `m`-variable.
    m.forEach((match, groupIndex) => {
        console.log(`Found match, group ${groupIndex}: ${match}`);
    });
}
