
/**
 * Expose regex.
 */

module.exports = /^function\s*\w*\s*\([\w\s,]*\)\s*{([\w\W]*?)}$/;


/**
 * get function name 
 */

`function multiply($number) {
    return ($number * 2);
}`.match(/([a-zA-Z_{1}][a-zA-Z0-9_]+)(?=\()/g)
